package org.medihub.application.ports.outgoing;

public interface AddClinicAdminPort {
    String addClinicAdmin();
}
