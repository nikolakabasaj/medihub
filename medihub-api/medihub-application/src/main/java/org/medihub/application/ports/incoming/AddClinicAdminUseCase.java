package org.medihub.application.ports.incoming;

public interface AddClinicAdminUseCase {
    String addClinicAdmin();
}
