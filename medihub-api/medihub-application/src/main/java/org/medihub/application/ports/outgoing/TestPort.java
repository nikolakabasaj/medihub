package org.medihub.application.ports.outgoing;

import org.medihub.domain.Test;

public interface TestPort {
    Test loadTest();
}
