package org.medihub.application.ports.incoming;

public interface TestUseCase {
    String doTest();
}
